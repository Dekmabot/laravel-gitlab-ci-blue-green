#!/bin/sh

PATH_PHP=$1
PATH_APP=$2
PATH_PUB=$3
PATH_PUB=$3

GREEN_PATH=$(realpath ${PATH_APP}/green)
BLUE_PATH=$(realpath ${PATH_APP}/blue)
CURRENT_DIR=$(readlink -f ${PATH_APP}/current)

#echo "php path: ${PATH_PHP}"
#echo "app dir: ${PATH_APP}"
#echo "pub dir: ${PATH_PUB}"

${PATH_PHP} -v

if [ "$CURRENT_DIR" = "$GREEN_PATH" ]; then
    NEXT_DIR=${BLUE_PATH}
else
    NEXT_DIR=${GREEN_PATH}
fi

echo "Step 2 - blue/green - ok, using $NEXT_DIR"

rm -rf $NEXT_DIR
mkdir $NEXT_DIR
cp -r ${PATH_APP}/tmp/. $NEXT_DIR

echo "Step 3 - copy - ok"

rm -rf $NEXT_DIR/storage $NEXT_DIR/logs

sh $NEXT_DIR/deploy/laravel.sh "${PATH_PHP}" "${PATH_APP}" "${NEXT_DIR}"

if [ -e ${PATH_PUB} ]; then
    rm -rf ${PATH_PUB}
fi

ln -snf $NEXT_DIR ${PATH_APP}/current
ln -snf ${PATH_APP}/current/public ${PATH_PUB}
ln -snf ${PATH_APP}/storage/app ${PATH_PUB}/storage

echo "Step 6 - public is ${PATH_PUB} - ok"
