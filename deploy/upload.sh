#!/bin/sh

echo "Step 1 - upload - start"

sshpass -p ${SSH_PASS} ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "rm -fr ${PATH_APP}/tmp && mkdir ${PATH_APP}/tmp/"
tar cf app.tar --exclude=./app.tar .
sshpass -p ${SSH_PASS} scp -P ${SSH_PORT} app.tar ${SSH_USER}@${SSH_HOST}:${PATH_APP}/tmp/
sshpass -p ${SSH_PASS} ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "cd ${PATH_APP}/tmp/ && tar xf app.tar && rm app.tar"

echo "Step 1 - upload - ok"

sshpass -p ${SSH_PASS} ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "sh ${PATH_APP}/tmp/deploy/deploy.sh '${PATH_PHP}' '${PATH_APP}' '${PATH_PUB}'"
