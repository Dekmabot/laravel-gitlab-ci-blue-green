#!/bin/sh

PATH_PHP=$1
PATH_APP=$2
NEXT_DIR=$3

#echo "php path: ${PATH_PHP}"
#echo "app path: ${PATH_APP}"
#echo "cur path: ${NEXT_DIR}"

mkdir -p ${PATH_APP}/storage
mkdir -p ${PATH_APP}/storage/framework
mkdir -p ${PATH_APP}/storage/framework/cache
mkdir -p ${PATH_APP}/storage/framework/sessions
mkdir -p ${PATH_APP}/storage/framework/views
mkdir -p ${PATH_APP}/logs

mkdir -p ${NEXT_DIR}/bootstrap/cache
ln -s ${PATH_APP}/storage ${NEXT_DIR}/storage
ln -sf ${PATH_APP}/logs ${NEXT_DIR}/storage/logs

echo "Step 4 - storage and logs dirs - ok"

${PATH_PHP} ${NEXT_DIR}/artisan migrate --force
${PATH_PHP} ${NEXT_DIR}/artisan config:cache
#${PATH_PHP} ${NEXT_DIR}/artisan storage:link

echo "Step 5 - migration and links - ok"
