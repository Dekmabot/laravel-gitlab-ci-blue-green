# Gitlab CI/CD template for Laravel project

This pack of scripts adds CI/CD to Laravel-based project, build and deploy it to remote hosting by SSH using blue-green schema, runs migrations, folder mapping and other stuff.

## How to install

1. Copy this source code to your project root, excluding .gitignore file.
2. Add folder paths and secrets to Gitlab Project -> Settings -> CI/CD -> Variables.
3. Push it to gitlab.

## What it does step by step
1. Uses php:8.1-alpine as a base image.
2. Adds common php-extensions.
3. Installs composer dependencies.
4. Installs npm dependencies.
5. Build front scripts.
6. Build .env file from template, using secrets from Gitlab CI/CI Variables from gitlab.com.
7. Tar and upload sources to remote server using SSH login/password.
8. Untar to ./blue or ./green folders by turn, symlink it to ./current after all.
9. Run Laravel migrations, symlink storage, logs, public endpoint folder and some other Laravel dirs.
10. Save your time for make some good things.

## Variable meanings

1. SSH_HOST, SSH_PORT, SSH_USER, SSH_PASS - ssh access credentials to upload code.
2. PATH_PHP - hosting absolute path to php executable, for example `/usr/local/php-cgi/8.1/bin/php`.
3. PATH_APP - hosting absolute path to your project root, for example `/home/d/username/my_cool_project`.
4. PATH_PUB - hosting absolute path to public server folder, for example `/home/d/username/my_cool_project/public_html`.
5. DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD - database credentials.
6. APP_ENV, APP_PORT, APP_URL, APP_DEBUG - common project settings.